import React, { useState, useEffect } from "react";
import Map from "./components/Map";

const defaultCenter = {
    lat: 25.0572,
    lng: 121.5375,
};

const defaultZoom = 15

export default function SaleMap() {
    const [storeInfoList, setStoreInfoList] = useState(null);

    useEffect(() => {
        fetch("https://app.angelsctek.com:8080/api/sales/get_visit_list", {
            method: "GET",
        })
            .then((result) => result.json())
            .then((data) => {
                let newStoreInfoList = [];

                data.result.map((storeInfo, index) => {
                    if (storeInfo.lat == null || storeInfo.lng == null) {
                        return;
                    }

                    let storeName = storeInfo.title;

                    let position = {
                        lat: storeInfo.lat,
                        lng: storeInfo.lng,
                    };

                    newStoreInfoList.push({
                        storeName: storeName,
                        address: storeInfo.address,
                        position: position,
                        image:storeInfo.store_img
                    });
                });

                setStoreInfoList(newStoreInfoList);
            });
    }, []);

    return (
        <div>
            {storeInfoList != null && (
                <Map
                    id="map"
                    defaultCenter={defaultCenter}
                    defaultZoom={defaultZoom}
                    storeInfoList={storeInfoList}
                />
            )}
        </div>
    );
}
