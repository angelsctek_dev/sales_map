import React from "react";
import styled from "styled-components";

const Container = styled.div`
    background: white;
    padding: 5px;
    border-radius: 10px;
    margin: 10px;
`

const Icon = styled.img`
    height: 20px;
    width: 20px;
`

function Legend(props) {
    return <Container id={props.id}>
        {
            props.legendList.map((legend, index)=>{
                return <div key={index}>
                    <Icon src={legend.icon} ></Icon>
                    <label>{legend.text}</label>
                </div>
            })
        }
    </Container>
}

export default Legend;
