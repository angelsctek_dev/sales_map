import React, { useEffect, useState, useRef} from "react";
import Legend from "./Legend";

const mouImage = "https://image.flaticon.com/icons/png/512/684/684908.png";
const checkInImage = "https://image.flaticon.com/icons/png/512/149/149060.png";

const legendList = [
    { text: "簽署mou", icon: mouImage },
    { text: "打卡", icon: checkInImage },
];

let currentOpenInfoWindow = null;

const DISPLAY_LABEL_ZOOM = 14;

function Map(props) {
    useEffect(() => {
        console.log("map useEffect");
        if (window.google) {
            console.log("map hahaha");
            return;
        }

        const script = document.createElement("script");
        script.src =
            "https://maps.google.com/maps/api/js?key=AIzaSyBqEyH5QzAh9Xe35afNgCSf07EnBtnSW-Y&callback=initMap";
        script.async = true;

        document.body.appendChild(script);

        return () => {
            console.log("map removeChild");
            document.body.removeChild(script);
        };
    }, []);

    // callback
    window.initMap = function () {
        const mapId = document.getElementById(props.id);
        const map = new window.google.maps.Map(mapId, {
            center: props.defaultCenter,
            zoom: props.defaultZoom,
            gestureHandling: "greedy",
            disableDefaultUI: true,
        });
        const markers = [];

        let labelPosition = new window.google.maps.Point(0, 40);
        let iconSize = new window.google.maps.Size(30, 30);
        props.storeInfoList.map((storeInfo) => {
            let icon;
            if (storeInfo.image != null) {
                icon = mouImage;
            } else {
                icon = checkInImage;
            }

            // marker
            let storeName = storeInfo.storeName;
            let marker = new window.google.maps.Marker({
                position: storeInfo.position,
                map,
                title: storeName,
                icon: {
                    url: icon,
                    labelOrigin: labelPosition,
                    scaledSize: iconSize,
                },
            });

            // infowindow
            let infoWindowContent =
                "<p> 店名：" +
                storeName +
                "<br/> 地址：" +
                storeInfo.address +
                "</p>";
            let infoWindow = new window.google.maps.InfoWindow();
            infoWindow.setContent(infoWindowContent);

            // add marker click listener
            marker.addListener("click", function () {
                if (currentOpenInfoWindow != null) {
                    currentOpenInfoWindow.close();
                }
                infoWindow.open(map, marker);
                currentOpenInfoWindow = infoWindow;
            });

            markers.push(marker);
        });

        // add map zoomChange listener
        // map.addListener("zoom_changed", function () {
        //     console.log("zoom: " + map.getZoom());
        //     if (map.getZoom() >= DISPLAY_LABEL_ZOOM) {
        //         markers.forEach((marker) => marker.setLabel(marker.title));
        //     } else {
        //         markers.forEach((marker) => marker.setLabel(null));
        //     }
        // });

        setUserPosition(map);
        initLegends(map);
    };

    // 設定使用者的位置為中心
    function setUserPosition(map) {
        // Try HTML5 geolocation.
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(
                function (position) {
                    var pos = {
                        lat: position.coords.latitude,
                        lng: position.coords.longitude,
                    };

                    map.setCenter(pos);
                },
                function (error) {
                    console.log("geolocation error message: " + error.message);
                }
            );
        } else {
            // Browser doesn't support Geolocation
            console.log("map: Browser does not support Geolocation");
        }
    }

    function initLegends(map) {
        var legend = document.getElementById("legend");

        map.controls[window.google.maps.ControlPosition.TOP_LEFT].push(legend);
    }

    return (
        
        <div>
            <Legend id="legend" legendList={legendList} />
            <div style={{ height: "100vh", width: "100%" }} id={props.id} />
        </div>
    );
}

export default Map;
