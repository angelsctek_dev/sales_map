import React, { useState, useEffect } from "react";
import GoogleMapReact from "google-map-react";

const centerPosition = {
    lat: 25.0572,
    lng: 121.5375,
};

const zoom = 15;

function SimpleMap() {
    // const [mapCenter, setMapCenter] = useState(centerPosition)

    const image =  "https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png";

    function renderMarkers(map, maps) {
        let labelPosition = new maps.Point(0,40)

        console.log("marker ");
        let marker = new maps.Marker({
            position: centerPosition,
            map,
            title: "title",
            icon:{
                labelOrigin: labelPosition,
                url: image
            }
        });

        map.addListener("zoom_changed", function(){
            if(map.getZoom() > 15){
                marker.setLabel("LABEL")
            }else{
                marker.setLabel(null)
            }
            console.log("zoom" + map.getZoom())
        })


        return marker;


        // fetch("https://app.angelsctek.com:8080/api/get_stores", {
        //     method: "GET",
        // })
        //     .then((result) => result.json())
        //     .then((data) => {
        //         data.result.map((storeInfo, index) => {
        //             let storeName = storeInfo.name;

        //             let infoWindow = new maps.InfoWindow({
        //                 content: storeName + "content",
        //             });

        //             let position = {
        //                 lat: storeInfo.lat,
        //                 lng: storeInfo.lng,
        //             };

        //             let marker = new maps.Marker({
        //                 position: position,
        //                 map,
        //                 title: storeName,
        //                 icon:{
        //                     labelOrigin: labelPosition,
        //                     url: image
        //                 },
        //                 label: { text: storeName, fontSize: "10px"},
        //             });

        //             marker.addListener("click", function () {
        //                 infoWindow.open(map, marker);
        //             });
        //         });
        //     });
    }

    return (
        // Important! Always set the container height explicitly
        <div style={{ height: "100vh", width: "100%" }}>
            <GoogleMapReact
                bootstrapURLKeys={{
                    key: "AIzaSyBqEyH5QzAh9Xe35afNgCSf07EnBtnSW-Y",
                }}
                defaultCenter={centerPosition}
                defaultZoom={zoom}
                onGoogleApiLoaded={({ map, maps }) => renderMarkers(map, maps)}
                yesIWantToUseGoogleMapApiInternals
            ></GoogleMapReact>
        </div>
    );
}

export default SimpleMap;
